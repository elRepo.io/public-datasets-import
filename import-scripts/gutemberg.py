'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''

import json
import os, os.path, argparse, sys
from gutemberg.gutemberg import getCachedFilesPath, downloadBookData, parseFullCatalog, parseOneBook
from gutemberg import updater
from utils.elrepoio_class import Elrepoio

CACHE_PATH = 'gutemberg/data.cache/cache/epub/'
CATALOG_URL = "https://www.gutenberg.org/cache/epub/feeds/rdf-files.tar.bz2"
CATALOG_ZIP = 'gutemberg/data.cache/rdf-files.tar.bz2'
CACHE_ROOT = 'gutemberg/data.cache/'

store = False

def elrepoioPrint(book):
    if store: downloadBookData(book, CACHE_PATH)
    # Return a local file path if the book is already downloaded
    formats = getCachedFilesPath(book, CACHE_PATH)

    content = book
    elrepoio = Elrepoio(
        book['title'],
        content,
        'http://www.gutenberg.org/ebooks/' + str(book['id']),
        formats['image/jpeg'] if 'image/jpeg' in book['formats'] else "",
        formats,
        book['subjects']
    )

    print(elrepoio.toJson())


def parseOne(number):
    res = parseOneBook(number, CACHE_PATH)
    elrepoioPrint(res)

def parseCatalog(limit=None, init=1,):
    # print("Runing parser catalog", store)
    # Limit of iterations is the number of folders
    if limit is None:
        limit = sum(os.path.isdir(CACHE_PATH + i) for i in os.listdir(CACHE_PATH))
    # print("With limits:", init, limit)
    # Check again that limits are correct
    initAndLimit(init, limit)
    for book in parseFullCatalog(CACHE_PATH, limit=limit, init=init):
        elrepoioPrint(book)

# Argument parser functions
def check_positive(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

# Used to check that init is <= limit. If one is None return True
def initAndLimit(init, limit):
    if init is not None and limit is not None and init > limit:
            raise argparse.ArgumentTypeError("Error: --init should be smaller or equal than --limit")
    return True

def main(argv):
    parser = argparse.ArgumentParser(description='Example:'
                                                 '\n\t* Parse catalog without download books:\n\t  run.py -p '
                                                 '\n\t* Download, extract, parse and store ebooks:\n\t  run.py -d -e -p -s'
                                                 '\n\t* Parse single ebook and download the files: \n\t  run.py -b 100 -s',
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-p", "--parse", help="Parse cached catalog on: '" + CACHE_PATH +
                        "'. Accept --store|s", action="store_true")
                        # "'. Accept --init, --limit, --store|s", action = "store_true")
    # parser.add_argument("--init", help="Used with --parse|-p. Set init", type=check_positive)
    # parser.add_argument("--limit", help="Used with --parse|-p. Set limit", type=check_positive)
    parser.add_argument("-b", "--book", help="Download single book. Give the number of the book in the catalog", type=check_positive,
                        nargs=None)
    parser.add_argument("-s", "--store", help="Used with --parse|-p or --book|-b. Store book on the drive", action="store_true")
    parser.add_argument("-d", "--download", help="Download RDF catalog from " + CATALOG_URL + " to default " + CATALOG_ZIP,
                        action="store_true")
    parser.add_argument("-e", "--extract", help="Extract " + CATALOG_ZIP + " to " + CACHE_ROOT + " or to specified dir",
                        action="store_true")

    args = parser.parse_args()

    if argv.__len__() == 0:
        parser.print_help(sys.stderr)
        sys.exit(0)

    global store
    store = args.store

    if args.book:
        parseOne(args.book)
    if args.download:
        updater.download(CATALOG_URL, download_path=CATALOG_ZIP)
    if args.extract:
        updater.extract(dest=CACHE_ROOT)
    if args.parse:
        # init = args.init or 1
        # limit = args.limit
        # initAndLimit(init, limit)
        # parseCatalog(init=init, limit=limit, )
        parseCatalog()

if __name__ == '__main__':
    main(sys.argv[1:])
