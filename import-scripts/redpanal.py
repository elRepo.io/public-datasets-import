'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''

import json
import argparse, sys
from redpanal.redpanal import RedPanal
from utils.elrepoio_class import Elrepoio

CACHE_PATH = 'redpanal/data.cache/'
update_cache=False
store = False

def elrepoioPrint(audio, filepath):
    content = audio
    elrepoio = Elrepoio(
        audio['name'],
        content,
        'https://redpanal.org/a/' + str(audio['slug']),
        "",
        {"file" : filepath},
        audio['tags']
    )
    print(elrepoio.toJson())

def getAudioDetails(id, rp):
    res = rp.getAudioDetails(id, update_cache=update_cache)
    if store: rp.downloadAudioFile(res, update_cache=update_cache)
    elrepoioPrint(res, rp.getCachedFilesPath(res))

# Argument parser functions
def check_positive(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

def main(argv):
    parser = argparse.ArgumentParser(description='Example:'
                                                 '\n\t* Download specific entry by id:\n\t  run.py -a 2235 -s',
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-a", "--audio", help="Download audio information by id or slug", nargs=None)
    parser.add_argument("-s", "--store", help="Used with --audio|-a . Store audio file on the drive", action="store_true")
    parser.add_argument("-c", "--cache", help="Update the cache data on search", action="store_true")

    args = parser.parse_args()

    if argv.__len__() == 0:
        parser.print_help(sys.stderr)
        sys.exit(0)

    global store
    store = args.store

    global update_cache
    update_cache = args.cache

    rp = RedPanal(CACHE_PATH)
    if args.audio:
        getAudioDetails(args.audio, rp)

if __name__ == '__main__':
    main(sys.argv[1:])
