'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''
from mediawiki import MediaWiki
import json, logging, tomd
from datetime import timedelta

class Wikimedia(MediaWiki):

    def __init__(self,
                 url="https://{lang}.wikipedia.org/w/api.php",
                 lang="en",
                 timeout=15.0,
                 rate_limit=False,
                 rate_limit_wait=timedelta(milliseconds=50),
                 cat_prefix="Category",
                 user_agent=None,
                 ):
        super().__init__(
                 url=url,
                 lang=lang,
                 timeout=timeout,
                 rate_limit=rate_limit,
                 rate_limit_wait=rate_limit_wait,
                 cat_prefix=cat_prefix,
                 user_agent=user_agent,
        )

    def getTotalPages(self):
        return self.getStatistics()['query']['statistics']['pages']

    def getStatistics(self, siprop='statistics'):
        return self.wiki_request({'meta': 'siteinfo', 'siprop': siprop})


    def getAllPages(self, cont=True):
        # cont is used to follow the pagination
        logging.debug("Get all page with continue=" + str(cont))
        apcontinue = True
        opts = {'list': 'allpages'}
        # Used iterative programming due to the python "limitation on maximum recursion depth".
        while apcontinue:
            res = self.wiki_request(opts)
            if 'allpages' in res['query']:
                for item in res['query']['allpages']:
                    yield item
            if cont is not False:
                apcontinue = res['continue']['apcontinue']
                opts['apcontinue'] = apcontinue
            else:
                apcontinue = False

    def getMarkdownContent(self, page):
        return tomd.convert(page['content'])
