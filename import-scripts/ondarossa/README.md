# Ondarossa importer

Used for import page data and metadata for [OndaRossa](http://ondarossa.info)
radio page.

Return metadata or podcast data of the page in json format.

## Use

```
usage: ondarossa.py [-h] [-a] [-t TYPE] [-m] [-p] [-u URL [URL ...]]
                    [--verbose]

Example:
	* Get all pages and print metadata:
	  python ondarossa.py -a -m
	* Get all pages from newsredazione:
	  python ondarossa.py -a -m -t newsredazione -v
	* Get specific page from url and print metadata
	  python ondarossa.py -m -u "http://www.ondarossa.info/newsredazione/2010/04/tentata-evasione-e-botte-nel-cie-torino -v"
	* Print podcast data for all pages:
	  python ondarossa.py -a -p

optional arguments:
  -h, --help            show this help message and exit
  -a, --all             Get all Ondarossa articles
  -t TYPE, --type TYPE  Specify the type of articles to get, for example
                        'newsredazione'
  -m, --metadata        Print page metadata
  -p, --podcast         Print podcast data
  -u URL [URL ...], --url URL [URL ...]
                        Import specific url pages
  --verbose, -v         Set logging to INFO with -v. Set logging to DEBUG with
                        -vv
```

All returning objects can be piped to `jq`.

## TODO's

* File download on specific format

