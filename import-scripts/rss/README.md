# RSS feed importer

The program parse an Rss feed url to get last posts summaries.

## Use

```
usage: run.py [-h] [-s] [--verbose] feedUrl [feedUrl ...]

Example:
	run.py https://url -s

positional arguments:
  feedUrl        Feed url to parse

optional arguments:
  -h, --help     show this help message and exit
  -s, --show     Show parsed posts
  --updates UPDATES  Get updates from specific new guid
  --verbose, -v  Set logging to INFO with -v. Set logging to DEBUG with -vv
```

## TODO's

* Support for radio podcasts and files

* Functions to store on a database.

* Support get updates. This could be done in two ways, for example storing last update time somewhere or using the 
database.

