'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''

import logging, feedparser, json
from utils.logger_utils import log

class Rss(object):

    def __init__(self, feed):
        logging.debug("Created RSS object for feed: " + feed)
        self.news = None
        self.feed = feed
        self.update(feedUrl=feed)

    def _feed_is_set(self):
        if self.feed is None:
            return False
        return True

    def _get_feedUrl_or_feed(self, feedUrl=None):
        if feedUrl is None:
            if not self._feed_is_set():
                raise Exception("No feed url specified")
            feedUrl = self.feed
        return feedUrl

    def update(self, feedUrl=None):
        self.news = self._parseFeed(feedUrl=feedUrl)
        return self.news

    @log
    def _parseFeed(self, feedUrl=None):
        feedUrl = self._get_feedUrl_or_feed(feedUrl=feedUrl)
        logging.info("Try to parse " + feedUrl)
        self.parsed = feedparser.parse(feedUrl)
        logging.info("Total RSS posts: " + str(len(self.parsed.entries)))
        return self.parsed

    @log
    def _is_last(self, guid, news):
        if guid in news.entries[0].guid:
            logging.info("Is already up to date!")
            return True
        return False

    @log
    def get_updates(self, lastGuid, feedUrl=None):
        feedUrl = self._get_feedUrl_or_feed(feedUrl=feedUrl)
        self.update(feedUrl=feedUrl)
        if not self._is_last(lastGuid, self.news):
            for new in self.news.entries:
                if new.guid in lastGuid:
                    return
                yield new

