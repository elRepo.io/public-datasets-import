# Wordpress importer

The objective of this scripts is create an easy way to import wordpress pages and posts.

To get data from wordpress sites we use the library [wp-api-python](https://github.com/derwentx/wp-api-python) 
installable via:

```
pip install wordpress-api   
```

## Use

```
Wordpress blogs parser:
usage: run.py [-h] [-p [POSTS]] [-a [PAGES]] [--verbose] [--summary]
              url [url ...]

Example:
	run.py https://noblogs.org --post -summary -v

positional arguments:
  url                   One ore more URL of wordpress blog (compatible with
                        APIv2, wordpress version > 4.7)

optional arguments:
  -h, --help            show this help message and exit
  -p [POSTS], --posts [POSTS]
                        Get all post or specific by id
  -a [PAGES], --pages [PAGES]
                        Get all pages or specific by id
  --verbose, -v         Verbose summaries or json responses. If summary
                        activated disable json response
  --summary, -s         Parse json to get a resume for each response
```


Where `--summary` parse the json and print a summary of the page or posts processed.

## TODO's

* Functions to store on a database.

* Update the database with last posts.