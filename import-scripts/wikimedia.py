'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''
import os
import sys, argparse, logging, json

from utils.elrepoio_class import Elrepoio
from wikimedia.wikimedia import Wikimedia
from utils.logger_utils import setLoging

CACHE_PATH = 'wikimedia/data.cache/'

def generateWikimediaCachePage(page):
    wikiCacheFile = CACHE_PATH
    if not os.path.exists(wikiCacheFile):
        os.makedirs(wikiCacheFile)
    path = wikiCacheFile + str(page.pageid) + ".html"
    with open(path, 'w') as outfile:
        outfile.write(page.html)
    return os.path.abspath(path)

def elrepoioPrint(page):
    elrepoio = Elrepoio(
        page.title,
        page.summary,
        page.url,
        "" if not page.logos else page.logos[0], # Get just the first logo as cover image
        {"file" : generateWikimediaCachePage(page)},
        [s[s.find(':')+1:] for s in page.categories] + page.redirects # Categories have a prefix like: ['Categoría:Pueblo mapuche'], substring this `:`
    )
    print(elrepoio.toJson())

def main(argv):
    parser = argparse.ArgumentParser(
        description='Pay attention to add language to get different langs:\n' +
                    'Example:\n' +
                    'wikimedia.py https://wikipedia.org/w/api.php -a -v\n' +
                    'wikimedia.py https://wikipedia.org/w/api.php -s "book" "ned ludd" -v\n' +
                    'wikimedia.py https://es.wikipedia.org/w/api.php -p "Pueblo Mapuche" -v',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('wikimediaUrl', metavar='wikimediaUrl', type=str, nargs='+',
                        help='Wikimedia Url to parse')
    parser.add_argument("-a", "--all", help="Get all pages", action='store_true')
    parser.add_argument("-s", "--search", help="Search pages that title contains ", type=str, nargs="+")
    parser.add_argument("-p", "--page", help="Search pages by name or slug", type=str, nargs="+")
    parser.add_argument('--verbose', '-v', action='count',
                        help="Set logging to INFO with -v. Set logging to DEBUG with -vv ")

    args = parser.parse_args()

    setLoging(args.verbose)

    for url in args.wikimediaUrl:
        wm = Wikimedia(url=url)
        if args.page:
            for p in args.page:
                logging.info("Getting page " + p)
                elrepoioPrint(wm.page(p))
        elif args.search:
            for s in args.search:
                logging.info("Looking for " + s)
                logging.info(wm.search(s,))

        elif args.all:
            logging.info("Trying to retrieve " + str(wm.getTotalPages()) + " pages")
            for page in wm.getAllPages():
                try:
                    logging.info(wm.page(page["title"]))
                except Exception as e:
                    logging.error("Error parsing: " + page["title"])
                    logging.error(e)

if __name__ == '__main__':
    main(sys.argv[1:])
