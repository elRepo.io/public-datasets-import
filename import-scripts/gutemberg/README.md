# Gutemberg project

After read the documentation of [gutemberg project](https://www.gutenberg.org/wiki/Gutenberg:Information_About_Robot_Access_to_our_Pages)
I found that the best way to extract the information of all the books is to 
download [RDF/XML](https://www.gutenberg.org/wiki/Gutenberg:Feeds#The_Complete_Project_Gutenberg_Catalog) 
and parse it locally to extract the information.

To parse it I get inspiration from [this](https://github.com/pravinyo/gutenberg-catalog-parsing)
script, so many thanks. 

## Use

```
usage: gutemberg.py [-h] [-p] [-b BOOK] [-s] [-d] [-e]

Example:
	* Parse catalog without download books:
	  run.py -p 
	* Download, extract, parse and store ebooks:
	  run.py -d -e -p -s
	* Parse single ebook and download the files: 
	  run.py -b 100 -s

optional arguments:
  -h, --help            show this help message and exit
  -p, --parse           Parse cached catalog on:
                        'gutemberg/data.cache/cache/epub/'. Accept --store|s
  -b BOOK, --book BOOK  Download single book. Give the number of the book in
                        the catalog
  -s, --store           Used with --parse|-p or --book|-b. Store book on the
                        drive
  -d, --download        Download RDF catalog from
                        https://www.gutenberg.org/cache/epub/feeds/rdf-
                        files.tar.bz2 to default gutemberg/data.cache/rdf-
                        files.tar.bz2
  -e, --extract         Extract gutemberg/data.cache/rdf-files.tar.bz2 to
                        gutemberg/data.cache/ or to specified dir

```

## TODO's

* Create an 'update' function that update the `RDF` file.
