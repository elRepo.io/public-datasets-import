'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''

'''
This file contain functions to parse the gutember.org RDF catalog.

Inspired by https://github.com/pravinyo/gutenberg-catalog-parsing/
'''

import os.path

import os
import re
import urllib

import requests
import xml.etree.cElementTree as ElementTree


try:
    import cPickle as pickle
except ImportError:
    import pickle

META_FIELDS = ('id', 'author', 'title', 'downloads', 'formats', 'type', 'LCC',
               'subjects', 'authoryearofbirth', 'authoryearofdeath', 'language')
NS = dict(
    pg='http://www.gutenberg.org/2009/pgterms/',
    dc='http://purl.org/dc/terms/',
    dcam='http://purl.org/dc/dcam/',
    rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#')
LINEBREAKRE = re.compile(r'[ \t]*[\n\r]+[ \t]*')
ETEXTRE = re.compile(r'''
    e(text|b?ook)
    \s*
    (\#\s*(?P<etextid_front>\d+)
    |
    (?P<etextid_back>\d+)\s*\#)
    ''', re.IGNORECASE | re.VERBOSE)


def fixsubtitles(title):
    """Introduce any subtitle with (semi)colons instead of newlines.
    The first subtitle is introduced with a colon, the rest with semicolons.
    fixsubtitles(u'First Across ...\r\nThe Story of ... \r\n'
    ... 'Being an investigation into ...')
    u'First Across ...: The Story of ...; Being an investigation into ...'"""
    tmp = LINEBREAKRE.sub(': ', title, 1)
    return LINEBREAKRE.sub('; ', tmp)

"""
https://gist.github.com/andreasvc/b3b4189120d84dec8857

{'LCC': ['PS'],
 'author': u'Burroughs, Edgar Rice',
 'authoryearofbirth': 1875,
 'authoryearofdeath': 1950,
 'downloads': 401,
 'formats': {'application/epub+zip': 'http://www.gutenberg.org/ebooks/123.epub.noimages',
  'application/prs.plucker': 'http://www.gutenberg.org/ebooks/123.plucker',
  'application/x-mobipocket-ebook': 'http://www.gutenberg.org/ebooks/123.kindle.noimages',
  'application/x-qioo-ebook': 'http://www.gutenberg.org/ebooks/123.qioo',
  'text/html; charset=iso-8859-1': 'http://www.gutenberg.org/files/123/123-h.zip',
  'text/plain': 'http://www.gutenberg.org/ebooks/123.txt.utf-8',
  'text/plain; charset=us-ascii': 'http://www.gutenberg.org/files/123/123.zip'},
 'id': 123,
 'language': ['en'],
 'rights': 'Copyrighted. Read the copyright notice inside this book for details.',
 'subjects': ['Adventure stories',
  'Earth (Planet) -- Core -- Fiction',
  'Fantasy fiction',
  'Science fiction'],
 'title': u"At the Earth's Core",
 'type': 'Text'} 
"""
def parseRdf(file):
    xml = ''
    if os.path.exists(file):
        xml = ElementTree.parse(file)
    else:
        xml = ElementTree.fromstring(requests.get(file).text)
    ebook = xml.find(r'{%(pg)s}ebook' % NS)
    if ebook is None:
        print("Error: Ebook is none")
        return

    result = dict.fromkeys(META_FIELDS)
    # get etext no
    about = ebook.get('{%(rdf)s}about' % NS)
    result['id'] = int(os.path.basename(about))
    # author
    creator = ebook.find('.//{%(dc)s}creator' % NS)
    if creator is not None:
        name = creator.find('.//{%(pg)s}name' % NS)
        if name is not None:
            result['author'] = name.text
        birth = creator.find('.//{%(pg)s}birthdate' % NS)
        if birth is not None:
            result['authoryearofbirth'] = int(birth.text)
        death = creator.find('.//{%(pg)s}deathdate' % NS)
        if death is not None:
            result['authoryearofdeath'] = int(death.text)
    # title
    title = ebook.find('.//{%(dc)s}title' % NS)
    if title is not None:
        result['title'] = fixsubtitles(title.text)
    # subject lists
    result['subjects'], result['LCC'] = [], []
    for subject in ebook.findall('.//{%(dc)s}subject' % NS):
        res = subject.find('.//{%(dcam)s}memberOf' % NS)
        if res is None:
            continue
        res = res.get('{%(rdf)s}resource' % NS)
        value = subject.find('.//{%(rdf)s}value' % NS).text
        if res == ('%(dc)sLCSH' % NS):
            result['subjects'].append(value)
        elif res == ('%(dc)sLCC' % NS):
            result['LCC'].append(value)

    # formats
    result['formats'] = {file.find('{%(dc)s}format//{%(rdf)s}value' % NS).text:
                             file.get('{%(rdf)s}about' % NS)
                         for file in ebook.findall('.//{%(pg)s}file' % NS)}
    # type
    booktype = ebook.find('.//{%(dc)s}type//{%(rdf)s}value' % NS)
    if booktype is not None:
        result['type'] = booktype.text
    # languages
    lang = ebook.findall('.//{%(dc)s}language//{%(rdf)s}value' % NS)
    result['language'] = [a.text for a in lang] or None
    # Rights
    rights = ebook.find('.//{%(dc)s}rights' % NS)
    if rights is not None:
        result['rights'] = fixsubtitles(rights.text)
    # download count
    downloads = ebook.find('.//{%(pg)s}downloads' % NS)
    if downloads is not None:
        result['downloads'] = int(downloads.text)

    return result


def parseFullCatalog(catalog_path, limit=None, init=1,):
    # Count of parsed books, each $cache will print it
    count = 0
    # Directory list (notice that the rdf directory tree is not with consecutive numbers, for example first 100 doesn't exist)
    listdir = list(map(int, os.listdir(catalog_path)))
    listdir.sort()

    for i in range(init, limit):
        file = catalog_path + str(listdir[i]) + "/pg" + str(listdir[i]) + ".rdf"
        count+=1
        # Parse RDF
        if os.path.isfile(file):
            yield parseRdf(file)

# Used to find the rdf of a single book without download the full catalog
def parseOneBook(number, CACHE_PATH):
    ebookRdfFile = str(number) + "/pg" + str(number) + ".rdf"
    # Check if file exist locally
    if os.path.exists(CACHE_PATH + ebookRdfFile ):
        return parseRdf(CACHE_PATH + ebookRdfFile)
    else:
        return parseRdf("https://www.gutenberg.org/cache/epub/" + str(number) + "/pg" + str(number) + ".rdf")

# Download book data from rdf parsed book
def downloadBookData(book, CACHE_PATH):
    for k,url in book['formats'].items():
        if not isDataDownloaded(book["id"], url, CACHE_PATH):
            try:
                urllib.request.urlretrieve(url, getDownloadFilename(book["id"], url, CACHE_PATH))
            # except urllib.request.HTTPError:
                # print('{"exception": "HTTPError Error trying to retrieve '+ url+'"}')
            except Exception:
                print()
                # print('{"exception": "Error trying to retrieve '+ url+' "}')


# Modify the url of a already downloaded file path and return array of it.
def getCachedFilesPath(book, CACHE_PATH):
    temp = book['formats'].copy()
    for k,url in temp.items():
        path = isDataDownloaded(book["id"], url, CACHE_PATH)
        if path is not None:
            temp[k] = isDataDownloaded(book["id"], url, CACHE_PATH)
    return temp

# Check if a file from an ebook is already downloaded,
# if it is return the path, else return False
def isDataDownloaded(bookId, url, CACHE_PATH):
    filename = getDownloadFilename(bookId, url, CACHE_PATH)
    return os.path.abspath(filename) if os.path.exists( filename ) else None

# Return the file name download path for a url file
def getDownloadFilename(bookId, url, CACHE_PATH):
    return CACHE_PATH + str(bookId) + "/" + correctEbookName(url.rsplit('/', 1)[1])

# Correct the name from kindel.images to images.kindle to let the readers open it
def correctEbookName(ebookName):
    if ".noimages" in ebookName:
        return ebookName.split('.',1)[0] + '.noimages.' + ebookName.replace('.noimages','').rsplit('.',1)[1]
    elif '.images' in ebookName:
        return ebookName.split('.',1)[0] + '.images.' + ebookName.replace('.images','').rsplit('.',1)[1]
    return ebookName
