'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''
import os
import sys, argparse, logging, json
from urllib.parse import urlparse
from urllib.request import urlretrieve

from activityPub.activitypub import ActivityPub
from utils.common import jsonDump, slugify, getDomain
from utils.elrepoio_class import Elrepoio
from utils.logger_utils import setLoging

CACHE_PATH = 'activityPub/data.cache/'

def generateNewPostCache(update):
    actorSlug = slugify(urlparse(update["actor"]).path)
    publicationSlug = update["object"]["id"][update["object"]["id"].rfind("/")+1:]
    path = CACHE_PATH + getDomain(update["actor"]) + "/" + actorSlug +  "/" + publicationSlug + "/"

    if not os.path.exists(path):
        os.makedirs(path)
    # else:
        # If cache exists just return
        # return

    # Create json with the new feed info
    with open(path + publicationSlug + ".json", 'w') as outfile:
        outfile.write(jsonDump(update))

    for media in update["object"]["attachment"]:
        fileUrlPath = urlparse(media["url"]).path
        fileName = fileUrlPath[fileUrlPath.rfind("/")+1:]
        if not os.path.exists(path + fileName):
            urlretrieve(media['url'], path + fileName)
        media['path'] = os.path.abspath(path + fileName)

    actorName = update["actor"][update["actor"].rfind('/') + 1:]

    return Elrepoio(
            "ActivityPub: " + actorName + " " + publicationSlug,
            update["object"]["content"],
            update["object"]["id"],
            "",
            update["object"]["attachment"],
            [tag["name"].replace("#", "") for tag in update["object"]["tag"]] + [actorName, getDomain(update["actor"])]
        )

def elrepoioPrint(updates):
    res = []
    for update in updates:
        newUpdate = generateNewPostCache(update)
        if newUpdate is not None:
            res.append(newUpdate)
    print(jsonDump(res))


def main(argv):
    parser = argparse.ArgumentParser(
        description='Example:'
                    '\n\t* Get 10 posts of ActivityPub address:'
                    '\n\t  run.py user@server.dom -a 10 -v'
                    # '\n\t* Get first 10 activity for given addresses and feeds url:'
                    # '\n\t  run.py user@server.dom user@server2.dom -f https://server.dom/users/user/outbox -a 10'
                    # '\n\t* Get user info on json format '
                    # '\n\t  run.py user@server.dom -u '
                    '\n\t* Get updates from specific post'
                    '\n\t  run.py user@server.dom --updates https://server.dom/users/user/statuses/postId ',
                    formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('address', metavar='address', type=str, nargs='*',
                        help='Ex: user@server.dom. One ore more ActivityPub address where to retrieve the info')
    # parser.add_argument("-u", "--user", help="Print user info in JSON format and exit. Only with address, not for feed urls.", action='store_true')
    # parser.add_argument("-f", "--feed", help="Parse feed url.", type=str, nargs='+')
    parser.add_argument("-a", "--activity", help="Show last activity for given address or feed urls. By default show all activity",
                        type=int)
    parser.add_argument("--first", help="Start parsing for first activity instead of last when parsing posts", action='store_true')
    parser.add_argument("--updates", help="Get updates from specific post id", type=str)
    parser.add_argument('--verbose', '-v', action='count', help="Set logging to INFO with -v. Set logging to DEBUG with -vv ")

    args = parser.parse_args()

    if argv.__len__() == 0:
        parser.print_help(sys.stderr)
        sys.exit(0)

    setLoging(args.verbose)

    logging.info("ActivityPub activity parser:")

    # TODO: check more beautiful way to do that on argparse docs
    limit = args.activity or None

    if args.address is not None:
        for person in args.address:
            logging.info("Getting info for " + person)
            ac = ActivityPub(address=person)
            # if args.user:
            #     print(json.dumps(ac.actor.user_json))
            #     sys.exit(0)
            if args.updates:
                print(json.dumps(ac.get_updates(args.updates)))
                sys.exit(0)
            elrepoioPrint(ac.get_feed_items(limit=limit, first=args.first))

    # if args.feed is not None:
    #     for feed in args.feed:
    #         logging.info("Getting info for feed url " + feed)
    #         ac = ActivityPub()
    #         if args.updates:
    #             print(json.dumps(ac.get_updates(args.updates, url=feed)))
    #             sys.exit(0)
    #         ac.get_feed_items(url=feed, limit=limit, first=args.first)

if __name__ == '__main__':
    main(sys.argv[1:])
