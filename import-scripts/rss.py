'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''
import os
import sys, argparse, logging, json
from urllib.request import urlretrieve

from rss.rss import Rss
from utils.common import slugify, getDomain, jsonDump
from utils.elrepoio_class import Elrepoio
from utils.logger_utils import setLoging
from urllib.parse import urlparse

CACHE_PATH = 'rss/data.cache/'

def generateNewPostCache(new):
    slug = slugify(urlparse(new.link).path)
    path = CACHE_PATH + getDomain(new.link) + "/" + slug +  "/"

    if not os.path.exists(path):
        os.makedirs(path)
    else:
        # If cache exists just return
        return

    # Create json with the new feed info
    with open(path + slug + ".json", 'w') as outfile:
        outfile.write(jsonDump(new))

    for media in new.media_content:
        fileUrlPath = urlparse(media["url"]).path
        fileName = fileUrlPath[fileUrlPath.rfind("/")+1:]
        if not os.path.exists(path + fileName):
            urlretrieve(media['url'], path + fileName)
        media['path'] = os.path.abspath(path + fileName)

    return Elrepoio(
            new.title,
            new.summary,
            new.link,
            new.image.href,  # Get just the first logo as cover image
            new.media_content,
            [tag.term for tag in new.tags] + [author.name for author in new.authors] + [new.credit]
        )

def elrepoioPrint(feed):
    res = []
    for new in feed.news.entries:
        newUpdate = generateNewPostCache(new)
        if newUpdate is not None:
            res.append(newUpdate)
    print(jsonDump(res))

def printPost(post):
    print("-----------------")
    print("\n" + post.title)
    print(post.published)
    print("\n" + post.summary)
    print("\nLink: " + post.link)

def main(argv):
    parser = argparse.ArgumentParser(
        description='Example:\n\trun.py https://url -s',
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('feedUrl', metavar='feedUrl', type=str, nargs='+',
                        help='Feed url to parse')
    parser.add_argument("-s", "--show", help="Show parsed posts", action='store_true')
    parser.add_argument("--updates", help="Get updates from specific new guid", type=str)
    parser.add_argument('--verbose', '-v', action='count',
                        help="Set logging to INFO with -v. Set logging to DEBUG with -vv ")

    args = parser.parse_args()

    setLoging(args.verbose)

    for url in args.feedUrl:
        rss = Rss(url)
        if args.updates:
            for update in rss.get_updates(args.updates):
                printPost(update)
        if args.show:
            # print("Total posts: ", len(rss.news.entries))
            elrepoioPrint(rss)
            # for post in rss.news.entries:
            #     printPost(post)

if __name__ == '__main__':
    main(sys.argv[1:])
