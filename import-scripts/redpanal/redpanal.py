'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''

import json
import os
from urllib.request import urlretrieve

from utils.request import Request

BASE_URL="https://redpanal.org"
AUDIO_DETAILS="/api/audio/{}/"
AUDIO_DETAILS_SLUG="/api/audio/by-slug/{}/"
AUDIO_LIST="/api/audio/list/"

slug_to_id = "slug_to_id.json"

class RedPanal(object):
	_cache_path = ""
	_slug_to_id = ""

	def __init__(self, cache_path,  request = None):
		self._cache_path = cache_path
		self._slug_to_id = cache_path + slug_to_id
		self._request = request or Request()

		# Initialize cache file
		if not os.path.exists(self._slug_to_id):
			with open(self._slug_to_id, 'w') as outfile:
				json.dump(dict(), outfile)

	# Get audio details from a request or from th cached file
	# If update_cache store the response in the cache
	def getAudioDetails(self, idOrSlug, update_cache=False):
		if isinstance(idOrSlug, int):
			id = idOrSlug
		else:
			id = self._getIdFromSlug(idOrSlug) if self._getIdFromSlug(idOrSlug) else idOrSlug
		if not os.path.exists(self._getAudioJsonCachePath(id)) \
				or update_cache:
			if isinstance(id, int):
				res = self._request.get(BASE_URL + AUDIO_DETAILS.format(id), timeout=None, )
			else:
				res = self._request.get(BASE_URL + AUDIO_DETAILS_SLUG.format(id), timeout=None, )
			self._storeCacheAudioData(res)
			return res
		else:
			with open(self._getAudioJsonCachePath(id)) as json_file:
				return json.load(json_file)

	def downloadAudioFile(self, audio, update_cache=False):
		if not os.path.exists(self._getAudioFileNameCachePath(audio)) or update_cache:
			urlretrieve(audio['audio'], self._getAudioFileNameCachePath(audio))

	# Update cache json data of specific audio
	def _storeCacheAudioData(self, audio):
		audioCachePath = self._getAudioCachePath(str(audio["id"]))
		if not os.path.exists(audioCachePath):
			os.makedirs(audioCachePath)
		with open(self._getAudioJsonCachePath(str(audio["id"])), 'w') as outfile:
			json.dump(audio, outfile)
		self._addSlugToId({audio["slug"] : audio["id"]})

	def _addSlugToId(self, slugToIdDict):
		with open(self._slug_to_id, "r+") as file:
			data = json.load(file)
			data.update(slugToIdDict)
			file.seek(0)
			json.dump(data, file)

	def _getIdFromSlug(self, slug):
		with open(self._slug_to_id, "r+") as file:
			data = json.load(file)
			try:
				return data[slug]
			except:
				return False

	# Get degault cache path
	def _getAudioCachePath(self, id):
		return self._cache_path + str(id)
	# Get json file cache path
	def _getAudioJsonCachePath(self, id):
		return self._getAudioCachePath(id) + "/" + str(id) + ".json"

	# Get audio file cache path from audio url object
	def _getAudioFileNameCachePath(self, audio):
		return self._getAudioCachePath(str(audio['id'])) + "/" + audio['audio'].rsplit('/', 1)[1]

	# Return the path of cached file or the original url of the file
	def getCachedFilesPath(self, audio):
		return os.path.abspath(self._getAudioFileNameCachePath(audio)) \
			if os.path.exists(self._getAudioFileNameCachePath(audio)) else audio['audio']





