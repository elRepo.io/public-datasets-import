```
Example:
	* Download specific entry by id:
	  run.py -a 2235 -s

optional arguments:
  -h, --help            show this help message and exit
  -a AUDIO, --audio AUDIO
                        Download audio information by int id
  -s, --store           Used with --audio|-a . Store audio file on the drive
  -c, --cache           Update the cache data on search
```