'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''
import json
import os
import sys, argparse
from urllib.parse import urlparse

from utils.elrepoio_class import Elrepoio
from wordPress.wordPress import Wrapper
CACHE_PATH = 'wordPress/data.cache/'

def wpToJson(wp):
    return json.dumps(wp)

def generateWikimediaCachePage(page):
    wordpressCacheFile = CACHE_PATH
    if not os.path.exists(wordpressCacheFile):
        os.makedirs(wordpressCacheFile)
    path = wordpressCacheFile + str(page['id']) + ".json"
    with open(path, 'w') as outfile:
        outfile.write(wpToJson(page))
    return os.path.abspath(path)

def elrepoioPrint(wp):
    elrepoio = Elrepoio(
        wp['title']['rendered'],
        wpToJson(wp),
        wp['guid']['rendered'],
        "", # Get just the first logo as cover image
        {"file" : generateWikimediaCachePage(wp)},
        []
    )
    print(elrepoio.toJson())


def main(argv):
    # print("Wordpress blogs parser:")

    parser = argparse.ArgumentParser(
        description='Example:'
                    '\n\trun.py https://noblogs.org --post -summary -v',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('url', metavar='url', type=str, nargs='+',
                        help='One ore more URL of wordpress blog (compatible with APIv2, wordpress version > 4.7)')
    parser.add_argument("-p", "--posts",
                        help="Get all post or specific by id",
                        type=int, const=True, nargs='?')
    parser.add_argument("-a", "--pages",
                        help="Get all pages or specific by id",
                        type=int, const=True, nargs='?')
    # parser.add_argument('--verbose', '-v', action="store_true", help="Verbose summaries or json responses. If summary "
    #                                                                  "activated disable json response")
    # parser.add_argument('--summary', '-s', action="store_true", help="Parse json to get a resume for each response")


    args = parser.parse_args()

    for url in args.url:
        wp = Wrapper(url)
        if args.posts:
            if args.posts is True:
                wp.getPosts()
            elif isinstance(args.posts, int):
                wp.getPosts(posts=args.posts)
            # if args.summary:
            #     wp.summaryPosts(verbose=args.verbose)
            # elif args.verbose:
            #     print(wp.posts)
            elrepoioPrint(wp.posts)
        if args.pages:
            if args.pages is True:
                wp.getPages()
            elif isinstance(args.pages, int):
                wp.getPages(pages=args.pages)
            # if args.summary:
            #     wp.summaryPages(verbose=args.verbose)
            # elif args.verbose:
            #     print(wp.pages)
            elrepoioPrint(wp.pages)

if __name__ == '__main__':
    main(sys.argv[1:])
