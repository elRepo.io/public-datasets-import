# Public datasets import

Add `--retroshare` on any script to import data to RetroShare node. Call scripts from `elrepo_cli`.

### Examples

- For Gutemberg project you have to download the [catalog](https://www.gutenberg.org/cache/epub/feeds/rdf-files.tar.bz2) and extract it on `import-scripts/gutemberg/data.cache`:

```bash
# Add single book downloading book file
dart bin/gutemberg.dart --retroshare -b 100 -s
# Parse full gutemberg catalog on import-scripts/gutemberg/data.cache/cache/epub downloading books files
dart bin/gutemberg.dart --retroshare -p -s
```

- For RedPanal you can only import single publications:

```bash
# Download audio with id or slug and import it to retroshare
dart bin/redpanal.dart --retroshare -s -a 2235
```

- Wikimedia support get pages by name or slug. It only support single pages import. The shared file is the wikimedia 
page in `.html` format 

```bash
dart bin/wikimedia.dart https://es.wikipedia.org/w/api.php -p "Pueblo Mapuche" --retroshare
```

- Wordpress can get pages or posts by id. It shares the resulting wordpress v2 API response as `.json` file containing 
the html of the publication (compatible with APIv2, wordpress version > 4.7).

```bash
dart bin/wordpress.dart https://noblogs.org -a 367 --retroshare
```

- Updates for RSS feeds can be imported using below command, if update available.

```bash
dart bin/rss.dart https://www.democracynow.org/podcast.xml -s --retroshare
```

- You can get updates for ActivityPub users using their direction. Just tested on Mastodon instances. Where `-a 10` is 
the number of updates to retrieve. 

```bash
dart bin/activityPub.dart komun_en@fosstodon.org -a 10 --retroshare
```

### Sumarizing

This series of scripts are used to import data from public datasets, such as Gutemberg project, or Redpanal, and import
the content into a RetroShare location, in this case, in elrepo.io publication format.

What a hell this mean? Lets see an example:

In one hand, a set of python scripts, for example, `gutemberg.py`. It takes an ebook, or the full library from Gutemberg 
project, it parses each ebook and prints a Json that will be consumed for the Dart scripts.

In the other hand, this dart scripts will process the python result and then create and publish a Post object on a
RetroShare node. This publication is planned to be used on elrepo.io project, so it will share a file and diferent 
hashtags related to it.

For example, for a gutemberg publication, on our elrepo.io app we will see an ebook file and lots of related hashtags, 
such as #ebook or #gutemberg.

## How to use it:

First configure your dart scripts to talk with your RS node on `lib/utils/constants.dart`

Just run (after `pub get`) the dart scripts. They will call automatically python scripts. So if we just do:

```
➜ dart bin/gutemberg.dart -h  

IMPORTANT: Add --retroshare as argument to store the imports in a retroshare forum

Run: guttemberg.py [-h]

usage: gutemberg.py [-h] [-p] [-b BOOK] [-s] [-d] [-e]

Example:
	* Parse catalog without download books:
	  run.py -p 
	* Download, extract, parse and store ebooks:
	  run.py -d -e -p -s
	* Parse single ebook and download the files: 
	  run.py -b 100 -s

optional arguments:
  -h, --help            show this help message and exit
  -p, --parse           Parse cached catalog on:
                        'gutemberg/data.cache/cache/epub/'. Accept --store|s
  -b BOOK, --book BOOK  Download single book. Give the number of the book in
                        the catalog
  -s, --store           Used with --parse|-p or --book|-b. Store book on the
                        drive
  -d, --download        Download RDF catalog from
                        https://www.gutenberg.org/cache/epub/feeds/rdf-
                        files.tar.bz2 to default gutemberg/data.cache/rdf-
                        files.tar.bz2
  -e, --extract         Extract gutemberg/data.cache/rdf-files.tar.bz2 to
                        gutemberg/data.cache/ or to specified dir
```

So, if we want to publish an ebook to elrepo.io we could just:

```
run.py --retroshare -b 100 -s
```

### Installation

You need Dart and python3.6 minimum.

```
cd ~/public-datasets-import/import-scripts
sudo apt install python3-pip python3-venv
python3.7 -m venv venv 
source venv/bin/activate
pip3 install -r requirments.txt 
```

```
cd ~/public-datasets-import/elrepoio_cli
dart pub get
```
