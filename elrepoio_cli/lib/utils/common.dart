/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
import 'dart:io';

import 'package:elrepo_lib/models.dart';
import 'package:elrepo_lib/repo.dart';
import 'package:elrepoio_cli/utils/constants.dart';
import 'package:elrepo_lib/retroshare.dart' as rs;

import '../models/publication_model.dart';

void initRetroshare() => rs.initRetroshare(
    identityId: AUTH.identityId,
    locationId: AUTH.locationId,
    passphrase: AUTH.passphrase,
    apiUser: AUTH.apiUser);

// Generic body post data to send to PublishPost
PostData createPostData(Publication pub,){
  if (pub.getFilePath().isNotEmpty &&
      FileSystemEntity.typeSync(pub.getFilePath()) == FileSystemEntityType.notFound) {
    throw ("File not found, please download the file first: " + pub.getFilePath());
  }
  var pd = PostData()
    ..filename = pub.getFileName()
    ..filePath = pub.getFilePath()
    ..metadata.contentType = pub.contentType
    ..metadata.title = pub.title
    ..metadata.summary = pub.summary
    ..project = pub.project
    ..originUrl = pub.originUrl;
  pd.mBody = PostBody()
    ..text = pub.getBodyPublication().toString()
    ..tags = pub.getTagsFormatted().cast<String>();
  return pd;
}

String getImportScriptsPath() => Directory.current.parent.path + "/import-scripts";

Future<ProcessResult> callPythonScript(scriptName, arguments){
  // Prepare to call gutemberg python script wit specific venv
  String importScriptsPath = getImportScriptsPath();
  String venvUrl = importScriptsPath + '/venv/bin/python';
  String scriptPath = importScriptsPath + "/"+ scriptName;
  List<String> args =  [scriptPath,]..addAll(arguments);
  Directory.current = importScriptsPath;

// Run python script with args
  return Process.run(venvUrl, args);
}

/// This function show information about client specific options
List<String> clientArgumentsParsing(List<String> arguments) {
  print('\nIMPORTANT: Add --retroshare as argument to store the imports in a retroshare forum\n');
  appConfig = AppConfig();
  if (arguments.contains('--retroshare')) {
    appConfig.saveToRS = true;
    return List<String>.from(arguments)
      ..removeWhere((element) => element.contains("--retroshare"));
  };
  return arguments;
}

AppConfig appConfig;

class AppConfig {
  bool _saveToRS = false;

  set saveToRS (bool save) => _saveToRS = save;
  bool get saveToRS => _saveToRS ;

}

/// For some publications stored on an html string, is usefull to delete html
/// tags to store it on the summary.
String removeAllHtmlTags(String htmlText) {
  RegExp exp = RegExp(
      r"<[^>]*>",
      multiLine: true,
      caseSensitive: true
  );

  return htmlText.replaceAll(exp, '');
}


