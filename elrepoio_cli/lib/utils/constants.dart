/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

class AUTH {
  static const String identityId = "bdbd397cf7800c5e085968e185633b50";
  static const String locationId = "814228577bc0c5da968c79272adcbfce";
  static const String passphrase = "test";
  static const String apiUser = "test";
}

class SupportedProjects {
  static const String redpanal = "RedPanal";
  static const String gutemberg = "Gutemberg";
  static const String wikimedia = "Wikimedia";
  static const String wordpress = "Wordpress";
  static const String rss = "Rss";
  static const String activityPub = "ActivityPub";
}
