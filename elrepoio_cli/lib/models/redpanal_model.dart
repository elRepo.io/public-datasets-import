/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
import 'dart:convert';
import 'package:elrepoio_cli/utils/constants.dart';
import 'package:elrepo_lib/models.dart' as models;
import 'publication_model.dart';

class RedpanalPublication extends Publication {

  @override
  String get project => SupportedProjects.redpanal;

  @override
  String get contentType => models.ContentTypes.audio;

  @override
  String get summary => content["description"];

  RedpanalPublication(title, content, originUrl, coverImage, files, tags,)
      : super (title, content, originUrl, coverImage, files, tags);

  RedpanalPublication.fromJson(Map<String, dynamic> json)
      : super.fromJson (json);

  List<dynamic> getTagsFormatted([List<String> moreTags]) =>
      super.getTagsFormatted(
          [this.content["instrument"],
            content["license"],
            content["use_type"],
            content["genre"],
            this.project
          ]);

  @override
  String getBodyPublication() {
    var res = Map.from(this.content)
      ..remove("id")
      ..remove("slug")
      ..remove("user")
      ..remove("totalframes")
      ..remove("samplerate")
      ..remove("use_type")
      ..remove("genre")
      ..remove("instrument")
      ..remove("tags")
      ..["project"] = project
      ..["originUrl"] = originUrl;
      return res.toString();
  }
}
