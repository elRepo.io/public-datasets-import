/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:meta/meta.dart';
import 'package:elrepo_lib/models.dart' as models;

/// Generic class used to import python scripts json data
abstract class Publication {
  final String title;
  final dynamic content;
  final String originUrl;
  final String coverImage;
  final dynamic files; // or List<String> or Map<String, dynamic>
  final List<dynamic> tags;

  /// Summary of publication (shown on the title of the forum)
  String get summary;

  String get project;

  String get contentType;


  Publication(this.title, this.content, this.originUrl, this.coverImage, this.files, this.tags,);

  Publication.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        content = json['content'],
        originUrl = json['originUrl'],
        coverImage = json['coverImage'],
        files = json['files'],
        tags = json['tags'];

  String _toCamelCase(String str) =>
      str
          ?.replaceAllMapped(
          RegExp(
              r'[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+'),
              (Match m) =>
          "${m[0][0].toUpperCase()}${m[0].substring(1).toLowerCase()}")
          ?.replaceAll(RegExp(r'(_|-|\s|,)+'), '') ?? str;
//    return s[0].toLowerCase() + s.substring(1); // Use dis to set first letter lower case

  List<dynamic> getTagsFormatted([List<String> moreTags]) =>
      [
        for (var tag in [ ...tags ?? [], ...?moreTags]) if (tag != null) "#" + _toCamelCase(tag)
      ];

  String getFilePath() =>
      this.files is List ? this.files[0] : this.files["file"];

  String getFileName() =>
      basename(new File(getFilePath()).path);

  /// Used to get a formated body publication, for example an html string, or
  /// the returned json from python scripts.
  ///
  /// This won't store tags or other stuff.
  String getBodyPublication();
}
