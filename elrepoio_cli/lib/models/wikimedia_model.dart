/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
import 'package:elrepo_lib/models.dart' as models;
import 'package:elrepo_lib/repo.dart' as repo;

import 'package:elrepoio_cli/utils/constants.dart';

import 'publication_model.dart';

class WikimediaPublication extends Publication {

  @override
  String get summary => repo.summaryFromBody(content);

  @override
  String get contentType => models.ContentTypes.text;

  @override
  String get project => SupportedProjects.wikimedia;

  WikimediaPublication(title, content, originUrl, coverImage, files, tags)
      : super (title, content, originUrl, coverImage, files, tags);

  WikimediaPublication.fromJson(Map<String, dynamic> json)
      : super.fromJson (json);

  List<dynamic> getTagsFormatted([List<String> moreTags]) =>
      super.getTagsFormatted([this.project]).toSet().toList();

  String getBodyPublication () {
    return content.toString();// todo: implement sanitization of body text
  }

}
