/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'package:elrepoio_cli/utils/constants.dart';

import 'publication_model.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:elrepoio_cli/utils/language_codes.dart' as lang;
import 'package:elrepo_lib/models.dart' as models;


class GutembergPublication extends Publication {

  @override
  String get project => SupportedProjects.gutemberg;

  @override
  String get contentType => models.ContentTypes.document;

  @override
  String get summary => this.content["author"];

  GutembergPublication(title, content, originUrl, coverImage, files, tags,)
      : super (title, content, originUrl, coverImage, files, tags,);

  GutembergPublication.fromJson(Map<String, dynamic> json)
      : super.fromJson (json);

  // Select one of the files of the gutemberg file array to be the published file
  String _getSeletedFile() =>
      super.files['application/epub+zip'] ??
          super.files['application/x-mobipocket-ebook'] ??
          super.files['text/plain'] ??
          false;

  String getFilePath() =>
      _getSeletedFile();

  String getFileName() =>
      basename(new File(_getSeletedFile()).path);

  List<String> _getLanguageNames() => [
    for (String language in this.content["language"])
      if (language?.isNotEmpty && lang.isoLangs[language] !=null)
        lang.isoLangs[language]["name"]
  ];


  List<dynamic> getTagsFormatted([List<String> moreTags]) =>
      super.getTagsFormatted(
          [...this._getLanguageNames(),
            this.content["author"],
            this.content["rights"],
            "Ebook" ,
            this.project,
          ]);

  @override
  String getBodyPublication () {
    var res = Map.from(this.content)
      ..remove("LCC")
      ..remove("downloads")
      ..remove("title")
      ..remove("id")
      ..remove("language")
      ..remove("type")
      ..["project"] = project
      ..["originUrl"] = originUrl;
    return res.toString();
  }
}
