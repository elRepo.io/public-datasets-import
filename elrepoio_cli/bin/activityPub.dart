/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'dart:io';
import 'package:elrepo_lib/models.dart';
import 'package:elrepo_lib/repo.dart';
import 'package:elrepoio_cli/models/activityPub_model.dart';
import 'package:elrepoio_cli/models/rss_model.dart';
import 'package:elrepoio_cli/models/wikimedia_model.dart';
import 'package:elrepoio_cli/models/wordpress_model.dart';
import 'package:elrepoio_cli/utils/common.dart' as common;
import 'package:elrepoio_cli/utils/constants.dart';

void main(List<String> arguments) {
  common.initRetroshare();
  arguments = common.clientArgumentsParsing(arguments);

  print("Geting updates and downloading files, this can be long...");

  // Run python script with args
  common.callPythonScript('activityPub.py', arguments).then((ProcessResult pr){
    dynamic json;
    try {
      json = jsonDecode(pr.stdout);
    } catch(e) {
      print("Error decoding JSON output");
    }
    if (json != null) {
      json.isNotEmpty ? print("Found ${json.length} updates") : print("No updates found");
      for(Map<String, dynamic> update in json) {
        ActivityPubPublication ap = ActivityPubPublication.fromJson(update);
        print("Update: ${ap.title}");
        print(ap.getFilePath());
        PostData pd = common.createPostData(ap);
        if (common.appConfig.saveToRS) publishPost(pd,);
      }
    }
    else {
      print(pr.stdout);
    }
  });
}
