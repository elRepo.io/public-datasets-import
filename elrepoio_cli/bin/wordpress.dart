/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'dart:io';
import 'package:elrepo_lib/models.dart';
import 'package:elrepo_lib/repo.dart';
import 'package:elrepoio_cli/models/wikimedia_model.dart';
import 'package:elrepoio_cli/models/wordpress_model.dart';
import 'package:elrepoio_cli/utils/common.dart' as common;
import 'package:elrepoio_cli/utils/constants.dart';

void main(List<String> arguments) {
  common.initRetroshare();
  arguments = common.clientArgumentsParsing(arguments);

  // Run python script with args
  common.callPythonScript('wordPress.py', arguments).then((ProcessResult pr){
    dynamic json;
    try {
      json = jsonDecode(pr.stdout);
    } catch(e) {
      print("Error decoding JSON output");
    }
    if (json != null) {
      WordpressPublication wp = WordpressPublication.fromJson(json);
      // print(wp.getBodyPublication());
      PostData pd = common.createPostData(wp);
      print(pd.mBody.tags);
      if (common.appConfig.saveToRS) publishPost(pd, parseTags: false);
    }
    else {
      print(pr.stdout);
    }
  });
}
