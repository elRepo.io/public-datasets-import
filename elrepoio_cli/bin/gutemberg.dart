/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'package:elrepo_lib/models.dart';
import 'package:elrepo_lib/repo.dart';
import 'package:elrepoio_cli/models/gutemberg_model.dart';
import 'package:elrepoio_cli/utils/constants.dart';
import 'package:path/path.dart';
import 'dart:io';

import 'package:elrepoio_cli/utils/common.dart' as common;
import 'package:elrepoio_cli/utils/common.dart';

Future<dynamic> runGuttembergPy(List<String> arguments) async {
  dynamic error;
  print("Run: guttemberg.py " + arguments.toString());
  if (arguments.contains("-s") || arguments.contains("--store")) print("-s defined: downloading book data");
  await common.callPythonScript('gutemberg.py', arguments).then((ProcessResult pr){
    try {
      dynamic json;
      try {
        json = jsonDecode(pr.stdout);
      } catch(e) {
        print("Error decoding JSON output: "+ pr.stdout);
      }
      if (json != null) {
        GutembergPublication ebook = GutembergPublication.fromJson(json);
        print("Received book:");
        print(ebook.getBodyPublication());
        var pd = common.createPostData(ebook);
        if (common.appConfig.saveToRS) publishPost(pd);
      }
      else{
        print(pr.stdout);
      }
    } catch(e){
      print("Error parsing " + arguments.toString());
      print(e);
      error = e;
    }
  });
  return error;
}

Future<void> main(List<String> arguments) async {
  common.initRetroshare();
  arguments = common.clientArgumentsParsing(arguments);

  // Pre process arguments to read big bunch of json
  // With -p the python script parse all the catalog.
  // The problem is that I didn't found an easy way to read long stdout,
  // so I process -p here instead of in the python script
  if (arguments.contains("-p") || arguments.contains("--parse")){
    // First remove -p
    var argsCopy = new List<String>.from(arguments)
      ..removeWhere((element) => element.contains("-p") || element.contains("--parse"));
    // Call the python script without -p, if we want to download the catalog or whatever
    // Don't handle errors
    if (argsCopy.isNotEmpty) {
      await common.callPythonScript('gutemberg.py', argsCopy).then((ProcessResult pr){
        print(pr.stderr);
        print(pr.stdout);
      });
    }
    // Finally parse the catalog book per book here instead of call parse in python script
    List<String> booksList = List<String>();
    String catalogPath = getImportScriptsPath() + "/gutemberg/data.cache/cache/epub";
    print("Retrieving catalog books list from: " + catalogPath);
    Directory(catalogPath)
      .list(recursive: false, followLinks: false)
        .listen((FileSystemEntity entity) {
          booksList.add(entity.path);
        })
    // I didn't achieve to do this inside the stream. I don't know how to use it with async functions
    // So it parses when onDone
        .onDone(() async {
          print("Catalog length: " + booksList.length.toString());
          Map<String, dynamic> errorMap = Map<String,String>();
          for (int  i = 0 ; i < booksList.length ; i++){
            String filename = basename(booksList[i]);
            print("Trying to parse: " + booksList[i] + " with bookId: " + filename);
            List<String> store = arguments.contains("-s") || arguments.contains("--store") ? ["-s"] : null;
            dynamic error = await runGuttembergPy(["-b", filename, ...?store ]);
            if (error != null) errorMap[filename] = error is String ? error: error.toString();
            print("Parsed " + i.toString() + " of " + booksList.length.toString());
            print(((i/booksList.length)*100).toString() + "% of total");
          }
          if (errorMap.isNotEmpty) {
            print("Finished with " + errorMap.length.toString() + " errors ");
            print(errorMap);
          }
    });
  }
  // Run python script with all args
  else {
    dynamic error = await runGuttembergPy(arguments);
    if (error != null) {
      print("Finished with errors ");
      print(error);
    }
  }
}
